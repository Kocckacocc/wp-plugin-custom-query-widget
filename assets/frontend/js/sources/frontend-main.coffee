(($) ->
# Use $ - - - - - - - - - - - - - - - - - - - - - - - -
	c = console.log
		
	getPosts = (filters, page, wrapper, clear)->
		$.ajax
			url : jsvars.ajax_url,
			type: 'POST',
			data:
				action : 'cqw'
				filters: filters
				page   : page
				
			success: (data) ->
				
				wrapper.parent().removeClass('cqw_hidden')
				
				if clear then wrapper.find('.cqw_page').remove()
				
				response = $('<div class="cqw_page cqw_page_' + page + '">' + data + '</div>')
				wrapper.find('.cqw_pager').before(response)
				wrapper.data('page', parseInt(page) + 1)
				###
				if response.find('.cqw_no_more').length != 0
					wrapper.find('.cqw_pager').addClass('cqw_hidden')
				else
					wrapper.find('.cqw_pager').removeClass('cqw_hidden')
				###

	docReady = ()->
		$.each $('.cqw_wrapper'), (id, wrapper) ->
			wrapper = $(wrapper)
			getPosts(wrapper.data('filters'), wrapper.data('page'), wrapper)
			
			wrapper.find('.cqw_pager a').on 'click.cqw', ()->
				getPosts(wrapper.data('filters'), wrapper.data('page'), wrapper)
		
		$.each $('.cqw_block'), (id, block) ->
			block = $(block)
			wrapper = block.find '.cqw_wrapper'
			filters = wrapper.data('filters')
			
			block.find('.cqw_year_filter a').on 'click.cqw', ()->
				
				block.find('.cqw_year_filter li').removeClass('active')
				$(@).parent().addClass('active')
				
				filters.year = $(@).data('year')
				getPosts(filters, 1, wrapper, true)
					
	$ ->
		
		$( '.fl-builder-content' ).on 'fl-builder.layout-rendered', ()->
			docReady()
		
		docReady()



# JQuery placeholder - - - - - - - - - - - - - - - - - - - - - - - -
) jQuery