<?php
/*
Plugin Name: Custom Query Widget
Description: A widget that shows custom posts, from custom query.
Version: 1.0.0
Author: Kocckacocc
*/

error_reporting( E_ALL );
ini_set( 'display_errors', 1 );

if ( ! defined( 'ABSPATH' ) )
	exit;

foreach ( ['main', 'widget'] as $class )
	require_once( 'includes/class-' . $class . '.php' );

require_once( 'includes/vendor/vendors.php' );

add_action( 'plugins_loaded', function () {
	\CQW\Main::instance();
} );

