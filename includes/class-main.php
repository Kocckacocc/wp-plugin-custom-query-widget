<?php

namespace CQW;

if ( ! defined( '\ABSPATH' ) )
	exit;

class Main {

	private static $_instance = null;

	public function __construct() {

		$this->_initial_config();

		$this->_load_textdomain();

		add_action( 'widgets_init', [ $this, 'register_widget' ] );

		add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_frontend_assets' ], 16, 1 );

		add_action( 'wp_ajax_cqw', array( $this, 'ajax_cqw' ) );
		add_action( 'wp_ajax_nopriv_cqw', array( $this, 'ajax_cqw' ) );

	}

	public function ajax_cqw() {

		$query = [
			'posts_per_page' => intval( $_REQUEST['filters']['post_per_page'] ),
			'paged'          => $_REQUEST['page'],
			'post_type'      => 'Events',

			'meta_key' => 'event_startdate',
			'orderby'  => 'meta_value_num',
			'order'    => 'DESC',

			'meta_query' => [
			]
		];

		$filters = $_REQUEST['filters'];

		if ( $filters['year'] != 'all' ) {

			$before = new \DateTime( $filters['year'] . '-01-01' );
			$before = date( 'Ymd', $before->getTimestamp() );

			$after = new \DateTime( $filters['year'] . '-12-31' );
			$after = date( 'Ymd', $after->getTimestamp() );

			$query['meta_query'][] = [
				'key'     => 'event_startdate',
				'compare' => '>=',
				'value'   => $before,
			];

			$query['meta_query'][] = [
				'key'     => 'event_startdate',
				'compare' => '<=',
				'value'   => $after,
			];

		}

		$today = date( 'Ymd' );

		$query['meta_query'][] = [
			'key'     => 'event_enddate',
			'compare' => $filters['status'] == 'closed' ? '<' : '>=',
			'value'   => $today,
		];

		$listings = new \WP_Query( $query );

		if ( $listings->found_posts > 0 ) {

			echo
				'<div class="cqw_response' . ( $listings->max_num_pages == $_REQUEST['page'] ? ' cqw_no_more' : '' ) . '">';

			while ( $listings->have_posts() ) {

				$listings->the_post();

				$fields = get_fields();

				if ( has_post_thumbnail() ) {
					$thumb = wp_get_attachment_image_src( get_post_thumbnail_id() );
				} else {
					$thumb = [
						'http://placehold.it/150x120',
						'150'
					];
				}

				if ( function_exists( 'pll_current_language' ) ) {
					$lang = pll_current_language( 'locale' );
				} else {
					$lang = 'hu_HU';
				}

				$date_formats = [
					'hu_HU' => 'Y. n d',
					'en_GB' => 'd n Y'
				];

				$months = [
					[ 'January', 'Január' ],
					[ 'February', 'Február' ],
					[ 'March', 'Március' ],
					[ 'April', 'Április' ],
					[ 'May', 'Május' ],
					[ 'June', 'Június' ],
					[ 'July', 'Július' ],
					[ 'August', 'Augusztus' ],
					[ 'September', 'Szeptember' ],
					[ 'October', 'Október' ],
					[ 'November', 'November' ],
					[ 'December', 'December' ]
				];

				$date_format = isset( $date_formats[ $lang ] ) ? $date_formats[ $lang ] : $date_formats['hu_HU'];

				$start_date = $fields['event_startdate'];
				$start_date = new \DateTime( $start_date );
				$start_date = $start_date->format( $date_format );

				$end_date = $fields['event_enddate'];
				$end_date = new \DateTime( $end_date );
				$end_date = $end_date->format( $date_format );

				$start_date = explode( ' ', $start_date );
				$end_date   = explode( ' ', $end_date );

				$monthkey      = $lang == 'hu_HU' ? 1 : 0;
				$start_date[1] = $months[ intval( $start_date[1] ) - 1 ][ $monthkey ];
				$end_date[1]   = $months[ intval( $end_date[1] ) - 1 ][ $monthkey ];

				foreach ( $end_date as $chk ) {
					foreach ( $start_date as $k => $v ) {
						if ( $v == $chk ) {
							if ( $lang == 'hu_HU' ) {
								if ( $k != 2 ) unset( $end_date[ $k ] );
							} else {
								if ( $k != 0 ) unset( $start_date[ $k ] );
							}
						}
					}
				}

				$delimeter = '-';

				if ( $lang != 'hu_HU' ) {
					$end_date[1] .= ',';
					if ( isset( $start_date[1] ) && isset( $start_date[2] ))
						$start_date[1] .= ',';

					if ( isset( $start_date[0] ) && isset( $start_date[1] ))
						$delimeter = ' - ';
				} else {
					if ( isset( $end_date[0] ) || isset( $end_date[1] ))
						$delimeter = ' - ';
				}

				$end_date   = implode( ' ', $end_date );
				$start_date = implode( ' ', $start_date );

				if ( $lang == 'hu_HU' ) {
					$end_date   = mb_strtolower( $end_date ) . '.';
					$start_date = mb_strtolower( $start_date );
				}

				echo
					'<div class="cl-event">' .

					'<div class="cl-event-entry media">' .
					'<div class="media-left">' .
					'<a href="' . $fields['event_url'] . '" target="_blank">' .
					'<img class="media-object" src="' . $thumb[0] . '" style="min-width: ' . $thumb[1] . 'px"/>' .
					'</a>' .
					'</div>' .

					'<div class="media-body">' .
					'<h3 class="media-heading">' .
					get_the_title() .
					'</h3>' .

					'<p>' .
					$start_date . $delimeter . $end_date . '</br>' .
					$fields['event_location'] . '</br>' .
					__( 'Pax:', TEXTDOMAIN ) . ' ' . $fields['event_pax'] .
					'</p>' .

					'<p>';

				if (
					isset( $fields['events_documents'] ) &&
					is_array( $fields['events_documents'] ) &&
					count( $fields['events_documents'] )
				) {

					$doc_label_helpers = [
						'elso_ertesito'       => __( 'First Announcement', TEXTDOMAIN ),
						'first announcement'  => __( 'First Announcement', TEXTDOMAIN ),
						'masodik_ertesito'    => __( 'Second Announcement', TEXTDOMAIN ),
						'second announcement' => __( 'Second Announcement', TEXTDOMAIN ),
						'jelentkezesi_lap'    => __( 'Registration Form', TEXTDOMAIN ),
						'registration form'   => __( 'Registration Form', TEXTDOMAIN ),
						'program'             => __( 'Program', TEXTDOMAIN ),
					];

					foreach ( $fields['events_documents'] as $attachment ) {

						echo
							'<a href="' . $attachment['event_document_file']['url'] . '" target="_blank">' .
							$doc_label_helpers[ mb_strtolower( $attachment['event_document_label'] ) ] .
							'</a>' . '</br>';
					}
				}

				echo
					'</p>' .
					'</div>' .
					'</div>' .
					'</div>';

			}

			echo '</div>';

			wp_reset_postdata();

		} else {

			echo '<h2 class="text-center qw_no_more">' . __( 'No events found...', TEXTDOMAIN ) . '</p>';

		}

		wp_die();
	}

	private function _initial_config() {

		define( 'CQW\ROOT_DIR', rtrim( plugin_dir_path( dirname( __FILE__ ) ), '/\\' ) );
		define( 'CQW\ROOT_URL', rtrim( plugin_dir_url( dirname( __FILE__ ) ), '/\\' ) );

		define( 'CQW\SHORT_NAME', 'cqw' );
		define( 'CQW\TEXTDOMAIN', 'cqw' );

	}

	private function _load_textdomain() {

		load_textdomain( TEXTDOMAIN, ROOT_DIR . '/languages/' . get_locale() . '.mo' );

	}

	public static function instance() {

		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	public function register_widget() {

		register_widget( '\CQW\Widget' );

	}

	public function enqueue_frontend_assets() {

		$this->_enqueue_styles( 'frontend' );

		wp_register_script(
			SHORT_NAME . '-frontend-main',
			ROOT_URL . '/assets/frontend/js/frontend-main.js',
			[ 'jquery' ],
			null,
			true
		);

		wp_localize_script( SHORT_NAME . '-frontend-main', 'jsvars', [ 'ajax_url' => admin_url( 'admin-ajax.php' ) ] );

		wp_enqueue_script( SHORT_NAME . '-frontend-main' );

	}

	private function _enqueue_styles( $where ) {
		foreach ( glob( ROOT_DIR . '/assets/' . $where . '/css/*.css' ) as $style_id => $style_path ) {
			wp_enqueue_style(
				SHORT_NAME . '-' . str_replace( '.css', '', basename( $style_path ) ),
				str_replace( ROOT_DIR, ROOT_URL, $style_path ),
				null,
				null
			);
		}

		wp_enqueue_style( 'animate', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css', null, null );

	}

	public function __clone() {
		_doing_it_wrong( __FUNCTION__, '...', null );
	}

	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, '...', null );
	}

}