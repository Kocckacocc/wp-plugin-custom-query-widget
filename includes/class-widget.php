<?php

namespace CQW;

class Widget extends \WP_Widget {

	function __construct() {

		parent::__construct(
			'cqw_widget',
			__( 'Events', TEXTDOMAIN ),
			array( 'description' => __( 'Displays events.', TEXTDOMAIN ) )
		);

	}

	function update( $new_instance, $old_instance ) {
		$instance                  = $old_instance;
		$instance['status']        = $new_instance['status'];
		$instance['year_filer']    = $new_instance['year_filer'];
		$instance['post_per_page'] = $new_instance['post_per_page'];

		return $instance;
	}

	function form( $instance ) {
		if ( $instance ) {
			$status        = $instance['status'];
			$year_filter   = $instance['year_filer'];
			$post_per_page = $instance['post_per_page'];
		} else {
			$status        = 'open';
			$year_filter   = 'enabled';
			$post_per_page = '-1';
		}

		echo
			'<p>' .
			'<label style="margin-right: 10px" for="' . $this->get_field_id( 'status' ) . '">' . __( 'Status: ', TEXTDOMAIN ) . '</label>' .
			'<select id="' . $this->get_field_id( 'status' ) . '" name="' . $this->get_field_name( 'status' ) . '">';

		$status_helper = [
			'open'   => __( 'Open', TEXTDOMAIN ),
			'closed' => __( 'Closed', TEXTDOMAIN ),
		];

		foreach ( [ 'open', 'closed' ] as $status_c ) {
			echo
				'<option ' . ( $status_c == $status ? 'selected="selected" ' : '' ) . 'value="' . $status_c . '">' .
				$status_helper[ $status_c ] .
				'</option>';
		}

		echo
			'</select>' .
			'</p>' .
			'<p>' .
			'<label style="margin-right: 10px" for="' . $this->get_field_id( 'year_filer' ) . '">' .
			__( 'Year filter: ', TEXTDOMAIN ) .
			'</label>' .
			'<select id="' . $this->get_field_id( 'year_filer' ) . '" name="' . $this->get_field_name( 'year_filer' ) . '">';

		$lang_helper = [
			'enabled'  => __( 'Enabled', TEXTDOMAIN ),
			'disabled' => __( 'Disabled', TEXTDOMAIN )
		];

		foreach ( [ 'enabled', 'disabled' ] as $year_filter_c ) {
			echo
				'<option ' . ( $year_filter_c == $year_filter ? 'selected="selected" ' : '' ) . 'value="' . $year_filter_c . '">' .
				$lang_helper[ $year_filter_c ] .
				'</option>';
		}

		echo
			'</select>' .
			'</p>' .
			'<p>' .
			'<label style="margin-right: 10px" for="' . $this->get_field_id( 'post_per_page' ) . '">' .
			__( 'Posts per page: ', TEXTDOMAIN ) .
			'</label>' .
			'<input style="width: 60px" id="' . $this->get_field_id( 'post_per_page' ) . '" name="' . $this->get_field_name( 'post_per_page' ) . '" type="number" min="-1" max="100" value="' . $post_per_page . '">' .
			'</p>';

	}

	function widget( $args, $instance ) {

		echo
		'<div class="cqw_block cqw_hidden row">';

		if ( ! isset( $instance['year_filer'] ) )
			$instance['year_filer'] = 'enabled';

		if ( $instance['year_filer'] == 'enabled' ) {

			$today = date( 'Ymd' );

			$query = [
				'posts_per_page' => - 1,
				'post_type'      => 'Events',
			];

			if ( ! isset( $instance['status'] ) )
				$instance['status'] = 'open';

			$query['meta_query'][] = [
				'key'     => 'event_enddate',
				'compare' => $instance['status'] == 'closed' ? '<' : '>=',
				'value'   => $today,
			];

			$listings = new \WP_Query( $query );

			if ( $listings->found_posts > 0 ) {

				$all = 0;
				while ( $listings->have_posts() ) {

					$listings->the_post();

					$year = substr( get_field( 'event_startdate', false, false ), 0, 4 );

					if ( isset( $years[ $year ] ) ) {
						$years[ $year ] ++;
					} else {
						$years[ $year ] = 1;
					}

					$all ++;

				}

				wp_reset_postdata();

				krsort( $years );

				$year_filter =
					'<div class="cqw_year_filter col-md-3">' .
					'<ul class="cl-event-nav nav nav-pills nav-stacked">';

				foreach ( $years as $year => $count ) {
					if ( ! isset( $not_first ) ) {
						$first_year = $year;
						$active     = ' class="active"';
					} else {
						$active = '';
					}
					$not_first = true;
					$year_filter .=
						'<li' . $active . '><a data-year="' . $year . '">' . $year . ' (' . $count . ')</a></li>';
				}

				$year_filter .=
					'</ul>' .
					'</div>';

			}

		}

		if ( ! isset( $instance['status'] ) )
			$instance['status'] = 'open';

		if ( ! isset( $instance['post_per_page'] ) )
			$instance['post_per_page'] = '-1';

		if ( ! isset( $first_year ) )
			$first_year = '2016';

		$filters = [
			'status'        => $instance['status'],
			'year'          => $first_year,
			'post_per_page' => $instance['post_per_page']
		];

		echo
			'<div class="cqw_wrapper' . ( isset( $year_filter ) ? ' col-md-9' : '' ) . '" data-filters=\'' . json_encode( $filters ) . '\' data-page="1">' .
			'<div class="cqw_pager cqw_hidden"><a class="btn btn-primary">' . __( 'More events', TEXTDOMAIN ) . '</a></div>' .
			'</div>';

		if ( isset( $year_filter ) )
			echo
			$year_filter;

		echo
		'</div>';

	}

}